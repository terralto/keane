Feature: Calculate the early retirement pension and lump sum factor.

Background: 
Given that today is "10/09/2015"

Scenario: A member retiring early, 
purchases notional service age to 65,
electing for deferred pension 
enters the scheme before April 2004

Given a member planning to retire on "13/09/2015"
And a date of birth of "03/03/1960" 
And a member elects to purchase notional service age to "65"
And entered the scheme on "01/01/1990"
Then early retirement pension factor is "58.2" 
Then early retirement lump sum factor is "82.4"
Then clean up 
 
Scenario: A member purchases notional service age to 65, enters the scheme before April 2004
Given a member planning to retire on "13/09/2015"
And a date of birth of "03/03/1960" 
And a member elects to purchase notional service age to "60"
And entered the scheme on "01/01/1990"   
Then early retirement pension factor is "77.80" 
Then early retirement lump sum factor is "90.70"
Then clean up
  
Scenario: A member purchases notional service age to 65, enters the scheme AFTER April 2004
Given a member planning to retire on "13/09/2015"
And a date of birth of "03/03/1960" 
And a member elects to purchase notional service age to "65"
And entered the scheme on "01/05/2004"   
Then early retirement pension factor is "58.20" 
Then early retirement lump sum factor is "82.40"
Then clean up

Scenario: A member purchases notional service age to 65 and will retire aged more than 60 
Given a member planning to retire on "13/09/2015"
And a date of birth of "03/03/1950" 
And a member elects to purchase notional service age to "65"
And entered the scheme on "01/05/1970" 
Then early retirement pension factor is "74.80" 
Then early retirement lump sum factor is "90.70"
Then clean up
 